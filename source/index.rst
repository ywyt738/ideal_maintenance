.. Ideal_Maintenance documentation master file, created by
   sphinx-quickstart on Wed Dec  7 09:55:59 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

欢迎来到理想公司集成部运维文档
=============================================

目录:

.. toctree::
   :maxdepth: 2
   :glob:

   installation_openssh
   git_guide
   cheatsheet
   Google_Authenticator
   chapters/*


.. Indices and tables
.. ==================
..
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
